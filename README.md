# Scaffold

Some basic functionalities you don't want to re-write for every new project.

It provides
- Routing
- Configuration
- Modules
- CLI (Command Line) Usage
- Low Execution Time
  - ~6ms without config cache (both web and cli)
- WebSockets (optional)

## Config

Config files can be placed in some locations, each with its own naming convention.

Let's say you want to configure some routes, you can place the config files in the following locations:

#### config/routes/

This will load the `global.php` and a `APP_ENV.php` from that directory, if they exist.

Some examples are:
- global.php
- testing.php
- production.php
- APP_ENV.php (where `APP_ENV` can be any string)

They are all treated as configs for `routes` beacause they are in a directory called `routes`

---

#### config/

Files placed here represent a config basen on their filename, so `config/routes.php`
is treated as config for `routes`

---

#### ModuleInterface::getConfig

Each module you define must implement the ModuleInterface and therefore the static method `getConfig`.

This method must return an array where you can give module specific configuration. The array is structurted like this:
```php
return [
    'routes' => [
        'module' => [
            'method'  => 'GET',
            'route'   => 'json/',
            'handler' => JsonRequestDebugController::class,
        ],
    ],
    'dependencies' => [
        SomeClass::class => \DI\factory(SomeClassFactory::class),
    ],
];
```

Each key of the arrays top layer is treated as its own configuration, so the key `routes` is for the routes config.

### Priority

Config entries can be overridden and merged, depinging on where they are defined.
Arrays are merged while simple values are overridden.

Here's the order in which configs are overridden (top is overridden by bottom):

- `ModuleInterface::getConfig`
- Single files in `config/`
- `config/some-config/gobal.php`
- `config/some-config/APP_ENV.php`

As you can see, the config from a module is the least prioritized one, so you can easily override parts of it.

#### Notice

Entries are overridden based on their keys.
You can use this when configuring to avoid or aim overrides for value-only arrays.

### Retrieving Config

The config is collected in a `ConfigCollection` Object. You can get it by adding it as a
dependency to your class (parameter of the `__construct` method).

When using a factory, you can simply get it from the container.

This object is read-only at runtime to prevent manipulation.

### Special Config Key

Config keys like `dependencies`, `routes`, `cli`, `processors` or `sockets` are used by this application.  
They have a special meaning for the application.

The `ConfigCollection` has these special keys defined as constants like `KEY_ROUTES`,
so they can be used in a less hard-coded way.

|Key         |Meaning                                            |
|------------|---------------------------------------------------|
|modules     |Registers, what modules should be loaded           |
|dependencies|Used as definitions for the dependency injector    |
|routes      |Defines the available routes                       |
|cli         |Defines the commands used via the command line     |
|processors  |Split into sub-arrays `pre` and `post`, see  *Processors* for more information|
|sockets     |WIP - Config used by the default `SocketController`|

---

## Modules

Modules help your application to be organized and separate logic.

Each module is defined by a class implementing `\FlyingAnvil\Scaffold\Application\Application\ModuleInterface`
and must implement its methods `getConfig`, which is called first on each module,
and `init`, which is called after the config has been built.

Modules also need to be registered in the `modules` config-key.
Note that modules *cannot* load other modules, so each module can only be registered using
your projects top level config.

Don't forget to register your modules namepaces in the `composer.json` for autoloading!

To ensure your modules override config from vendor modules, add your modules after them to the `modules` config.

---

## Processors

*Only available for non-cli controllers*  
They are separated into pre- and post-processors, which are called before or after a controller.

Pre-Processors must implement `\FlyingAnvil\Scaffold\Application\Processor\PreProcessorInterface` while
Post-Processors must implement `\FlyingAnvil\Scaffold\Application\Processor\PostProcessorInterface`.

They are configured using the config-key `processors` and the sub-keys `pre` and `post` respectively.
`pre` and `post` both are just simple lists of class names.

Pre-Processers can stop the execution of other pre-processors and can override the controller class.

There are also two special post-processors for error handling. Instead of using the key `post`, they use the
special key `error`, or `\FlyingAnvil\Scaffold\Application\Processor\ErrorProcessorInterface::KEY_ERROR`.
The value of the `error` key is another array with the keys `404` and `405`,
or the other constants of the interface.
They are used when no route matches the request uri or when the request method is not allowed.

Example use-cases are caching, where a response can be stored by a post-processor
and a pre-processor overrides the controller class, when a cached response can be loaded.  
Authentification and authorization can also be done usnig Pre-Processors.

---

## Routes

### Basic definition

Choose a way to define your route that fits best the way you need.
When a route belongs to a module, define it at your modules config.
It is a general route? Define it at `config/routes.php` or `config/routes/global.php`.
It's a debug route that should only be available in your testing environment? Define it at `config/routes/testing.php`

A route config is an array which consists of these base parts:
- the key of the array defines the route and its name
- `method`, wich is the HTTP request method (like `GET` or `POST`)
- `handler`, class name of the controller, that will be used

Example:
```php
'login' => [
    'method'  => 'GET',
    'handler' => LoginController::class,
],
```

This will result in a route named `login`, which is accessible at `/login` via the `GET` method
and will be processed by the `LoginController`.

### Overriding route and name

However, a routes name and route can be overridden (useful when nesting routes):
```php
'login' => [
    'method'  => 'GET',
    'route'   => 'user/login'
    'handler' => LoginController::class,
    'name'    => 'get-login-route',
],
```

This will result in a route named `get-login-route`, which is accessible at `/user/login` via the `GET` method
and will be processed by the `LoginController`.

### Nested routes

Route definitions can also be nested in order to group them a bit. When nesting routes, only the "leaf" of the array
is used to create a route, but the parent elements help with the naming.

```php
'user' => [
    'login' => [
        'method'  => 'GET',
        'handler' => LoginController::class,
    ],
    'register' => [
        'method'  => 'GET',
        'handler' => RegisterController::class,
    ],
],
```

This example results in two routes:
1. `/user/login`
2. `/user/register`

---

You can do a bit more with overriding when using nested routes

```php
'user' => [
    'login' => [
        'method'  => 'GET',
        'handler' => LoginController::class,
        'route'   => 'signin'
    ],
    'register' => [
        'method'  => 'GET',
        'handler' => RegisterController::class,
        'route'   => '/register'
    ],
],
```

This example results in two routes:
1. `/user/signin`
2. `/register`

The overriding value replaces the one from the parent element.
If the value begins with a slash (`/`), all parent values are ignored.

The `name` can be overridden the same way as `route`.

### Optional Segments

Wrap route segments in square brackets (`[…]`) to make it optional (works only at the end of a route)
```php
'debug' => [
    'request[/{format}]' => [
        'method'  => 'GET',
        'handler' => RequestDebugController::class,
    ],
],
```

This exaple makes requests to both `/debug/request` and `/debug/request/json` possible.

#### Notice

Overriding relative `route` and `name` while using optional segments is currently broken.
Absolute overrides (`'route' => '/some/route`) work though

### Route Parameters

Rotes can contain placeholders that can be passed to the controller as the only parameter for the `handle` method.
It is a `VarsCollection`, which is read-only at runtime to prevent manipulation.

```php
'user' => [
    'profile' => [
        '{name}' => [
            'method'  => 'GET',
            'handler' => JsonRequestDebugController::class,
        ]
    ]
],
```

This will create the route `/user/profile/{name}`, where `{name}` can be any alphanumeric string. 

Requests to `/user/profile/gramini` or `/user/profile/nomeless_guy` are both possible.

The content of these placeholders are passed as associative array to the controller as its only parameter.

You can of course have multiple placeholders in a route.

```php
'user' => [
    '{name}' => [
        'statistics' => [
            '{type}' => [
                'method'  => 'GET',
                'handler' => JsonRequestDebugController::class,
                'route'   => '{type}/{date}',
            }
        ]
    ]
],
```

This will create the route `/user/{name}/statistics/{type}/{date}` for requests like
- `/user/gramini/statistics/active-sessions/2019-08-11`
- `/user/charles/statistics/activities/2019-06-10`
- `/user/bastian/statistics/i_dont_know/all`

Don't overuse it, as you could (and probably should) use query parameters. But hey, it's possible.

---

## Controller

Controller are classes that handle a request. They process the incoming data (if any) and generate an output.
Each route can have its own controller or the same controller can be used for multiple routes.

A Controller needs to implement from `\FlyingAnvil\Scaffold\Application\Controller\ControllerInterface` or
extend `\FlyingAnvil\Scaffold\Application\Controller\AbstractController`, which provides some helpful methods
your controller can use (like `redirect` or `jsonResponse`).

This method has only one parameter, `$vars`, which contains the placeholders from the route
(or is empty if there are no placeholders, of course).

The request can be accessed using the `getRequest` method. It contains information about
query parameters, body parameters, cookies, uplodad files, ip, accept header, referer and so on.

To output something, you have to use the `response`, which can be gotten using the `getResponse` method.
Using this response you can set headers, send cookies, set the status code, redirect and,
most importantly, set the content.

You can get a PHP-renderer from `getRenderer`, which renders a .php file (template) into text.
It also accepts params that can be used inside of the .php file (template).

Finally there is a helper method for outputting json (`jsonResponse`), which takes an array,
automatically encodes it and puts the `Content-Type` header for json.

For examples, look at some of these debug controllers
- JsonConfigDebugController
- JsonRequestDebugController
- PlainRequestDebugController
- RouteDebugController
- VarDumpRequestDebugController

---

## CLI

You can also create some command line usages.  
Start by configuring a `cli` config:
```php
'ConfigCollection::KEY_CLI' => [
    'your-command' => [
        'description' => 'Dummy command for testing purposes, not doing anything',
        'handler'     => TestCliController::class,
        'params'      => [
            'date' => [
                CliParams::KEY_SHORT       => 'd',
                CliParams::KEY_LONG        => 'date',
                CliParams::KEY_MODIFIER    => CliParams::MOD_REQUIRED,
                CliParams::KEY_DESCRIPTION => 'Some random date which is ignored' .
                    ' (Does not need to be a date either)',
                CliParams::KEY_DEFAULT     => null,
            ],
        ]
    ],
],
```

As you can see, a command definition can be a little big, but basically you give the name of the command
as a key, an array as its value. A command needs a description, which is used for the output of `help`,
and a handler, which must implement `\FlyingAnvil\Scaffold\Application\Controller\CliControllerInterface`
or extend `\FlyingAnvil\Scaffold\Application\Controller\AbstractCliController`.

When extending `AbstractCliController`, you can override the `getHelp` method to format the help manually.

There's a defaul command, `help`, which lists all available commands with their
description and a list of accepted parameters.  
Use `help name-of-some-command` to call the `getHelp` method of the command handler. If not overridden,
it will print the commands description as well as each parameter with its description, formatted pretty

The handling Cli-Controller will get the params as `CliParams` obejct and a `CliOutput` object,
wich provides some useful methods and should be used for printing to stdout.

---

## WebSockets

// TODO WIP, mabye later

// have fun documenting it…

---
