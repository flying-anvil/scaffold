<?php

use FlyingAnvil\Scaffold\Application\Application\CliApplication;
use FlyingAnvil\Scaffold\Application\Collection\ConfigCollection;
use FlyingAnvil\Scaffold\Application\Controller\CliHelpController;
use FlyingAnvil\Scaffold\Application\Processor\ErrorProcessor404;
use FlyingAnvil\Scaffold\Application\Processor\ErrorProcessor405;
use FlyingAnvil\Scaffold\Application\Processor\ErrorProcessorInterface;

return [
    ConfigCollection::KEY_PROCESSORS => [
        ErrorProcessorInterface::KEY_ERROR => [
            ErrorProcessorInterface::KEY_404 => ErrorProcessor404::class,
            ErrorProcessorInterface::KEY_405 => ErrorProcessor405::class,
        ]
    ],
    ConfigCollection::KEY_CLI => [
        CliApplication::KEY_HELP_AND_COMMAND_NOT_FOUND => [
            'handler' => CliHelpController::class,
        ],
    ],
];
