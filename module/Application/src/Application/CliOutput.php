<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Application;

class CliOutput
{
    /** @var string[] */
    private $storedMessages = [];

    public function write(string $text): self
    {
        echo $text;
        return $this;
    }

    public function writeColor(string $text): self
    {
        if (!class_exists(Color::class)) {
            var_dump('no oclor');
            return $this;
        }

        $this->write($text);

        return $this;
    }

    public function writeLine(string $text = ''): self
    {
        echo $text, PHP_EOL;
        return $this;
    }

    public function writeUnderline(string $text, string $decoration = '⎺'): self
    {
        $textLength       = mb_strlen($text);
        $decorationLength = mb_strlen($decoration);
        $repeats          = (int)($textLength / $decorationLength);

        $this->writeLine($text);
        $this->writeLine(
            str_repeat($decoration, $repeats) .
            substr($decoration, 0, $textLength - ($repeats * $decorationLength))
        );

        return $this;
    }

    public function writeRecursive($message): self
    {
        print_r($message);
        return $this;
    }

    public function storeMessage(string $message): self
    {
        $this->storedMessages[] = $message;
        return $this;
    }

    public function getStoredMessages(): array
    {
        return $this->storedMessages;
    }
}
