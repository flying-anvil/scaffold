<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Application;

use FlyingAnvil\Scaffold\Application\Collection\ConfigCollection;
use FlyingAnvil\Scaffold\Application\Config\ConfigBuilder;
use FlyingAnvil\Scaffold\Application\ErrorHandler\WhoopsLoader;
use FlyingAnvil\Scaffold\Application\Router\RouteConfigToArray;
use FlyingAnvil\Scaffold\Application\Router\RouteDispatcher;
use DI\Container;

class Application extends BaseApplication
{
    /** @var ExtendedHttpRequest */
    private $request;

    /** @var ExtendedHttpResponse */
    private $response;

    public function __construct(array $get, array $post, array $cookies, array $files, array $server)
    {
        $this->request  = new ExtendedHttpRequest($get, $post, $cookies, $files, $server);
        $this->response = new ExtendedHttpResponse();
    }

    public function run(): void
    {
        $errorHandlerLoader = new WhoopsLoader();
        $errorHandlerLoader->load();

        $configBuilder = new ConfigBuilder();
        $config        = $configBuilder->build();

        $this->applyModuleConfig($config->get('modules', []), $config);
        $config = $config->lock();

        $container = $this->buildDependencyContainer($config);

        $this->initModules($config, $container);
        $this->dispatchRoute($config, $container);
        $this->printResponse();
    }

    private function dispatchRoute(ConfigCollection $config, Container $container): void
    {
        $routeConverter = new RouteConfigToArray();
        $routes = $routeConverter->convert($config->get(ConfigCollection::KEY_ROUTES));

        $router = new RouteDispatcher($config);

        foreach ($routes as $route) {
            $router->addRoute($route);
        }

        $router->build();
        $router->dispatch($container, $this->request, $this->response);
    }

    private function printResponse(): void
    {
        if (!headers_sent()) {
            foreach ($this->response->getHeaders() as $header) {
                header($header);
            }
        }

        echo $this->response->getContent();
    }
}
