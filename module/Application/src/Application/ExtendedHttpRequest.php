<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Application;

use FlyingAnvil\Scaffold\Application\Collection\GenericCollection;
use Http\HttpRequest;
use Http\MissingRequestMetaVariableException;

class ExtendedHttpRequest extends HttpRequest
{
    /** @var string[] */
    private $headers;

    public function __construct(
        array $get,
        array $post,
        array $cookies,
        array $files,
        array $server,
        $inputStream = ''
    ) {
        parent::__construct($get, $post, $cookies, $files, $server, $inputStream);

        $this->headers = GenericCollection::create();

        foreach ($server as $item => $value) {
            if (strpos($item, 'HTTP_') === 0) {
                $headerName = mb_strtolower(substr($item, 5));
                $this->headers->add($value, $headerName);
            }
        }

        $this->headers->lock();
    }

    public function getIpAddress()
    {
        $remoteAddr = $this->getServerVariable('REMOTE_ADDR');

        if (strpos($remoteAddr, '172') === 0) {
            $remoteAddr = $this->getHeader('X_REAL_IP');
        }

        return $remoteAddr;
    }

    public function getHeaders(): GenericCollection
    {
        return $this->headers;
    }

    public function getHeader($key)
    {
        return $this->headers->get(mb_strtolower($key));
    }

    protected function getServerVariable($variableName)
    {
        if (!array_key_exists($variableName, $this->server)) {
            throw new MissingRequestMetaVariableException($variableName);
        }

        return $this->server[$variableName];
    }
}
