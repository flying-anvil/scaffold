<?php

namespace FlyingAnvil\Scaffold\Application\Application;

interface ModuleInterface
{
    public static function getConfig(): array;

    public function init(): void;
}
