<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Application;

use Http\HttpResponse;

class ExtendedHttpResponse extends HttpResponse
{
    public function json(array $data): self
    {
        $this->setContent(json_encode($data, JSON_THROW_ON_ERROR));
        $this->setHeader('Content-Type', 'application/json');
        return $this;
    }
}
