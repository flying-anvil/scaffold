<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Application;

use DI\Container;
use function DI\get;
use FlyingAnvil\Scaffold\Application\Collection\CliParams;
use FlyingAnvil\Scaffold\Application\Collection\ConfigCollection;
use FlyingAnvil\Scaffold\Application\Collection\LockedConfigCollection;
use FlyingAnvil\Scaffold\Application\Config\CliParamsBuilder;
use FlyingAnvil\Scaffold\Application\Config\ConfigBuilder;
use FlyingAnvil\Scaffold\Application\Controller\AbstractCliController;
use FlyingAnvil\Scaffold\Application\Controller\CliControllerInterface;
use Psr\Container\ContainerInterface;

class CliApplication extends BaseApplication
{
    public const KEY_HELP_AND_COMMAND_NOT_FOUND = '_commandNotFound';

    /** @var array */
    private $cliArguments;

    /** @var CliOutput */
    private $output;

    public function __construct(array $cliArguments)
    {
        $this->cliArguments = $cliArguments;
        $this->output       = new CliOutput();
    }

    public function run(): void
    {
        $configBuilder = new ConfigBuilder();
        $config        = $configBuilder->build();

        $this->applyModuleConfig($config->get('modules', []), $config);
        $config->merge(ConfigCollection::KEY_DEPENDENCIES, [
            CliOutput::class => $this->output,
            'output'         => get(CliOutput::class),
        ]);
        $config = $config->lock();

        $container = $this->buildDependencyContainer($config);

        $this->initModules($config, $container);
        $this->dispatchCommand($config, $container);
        $this->printRemainingOutput();
    }

    private function dispatchCommand(LockedConfigCollection $config, Container $container): void
    {
        $command      = $this->cliArguments[1] ?? '';
        $originalCmd  = $command;
        $allCommands  = $config->get(ConfigCollection::KEY_CLI, []);
        $cmdOptions   = $allCommands[$command] ?? null;

        // Get Help
        if ($command === 'help') {
            $this->showHelp($container, $allCommands);
            return;
        }

        // no or invalid command
        if ($command === '' || $cmdOptions === null) {
            $command      = self::KEY_HELP_AND_COMMAND_NOT_FOUND;
            $cmdOptions   = $allCommands[$command] ?? [];
            $handlerClass = $cmdOptions['handler'] ?? '';

            $handler = $this->getCommandHandler($handlerClass, $container, $command);
            $this->handleCommandNotFound($originalCmd, $allCommands, $handler);

            return;
        }

        unset($allCommands[self::KEY_HELP_AND_COMMAND_NOT_FOUND]);
        $handlerClass = $cmdOptions['handler'] ?? '';
        $handler      = $this->getCommandHandler($handlerClass, $container, $command);
        $paramsConfig = $cmdOptions['params'] ?? [];

        try {
            $paramsBuilder = new CliParamsBuilder();
            $params = $paramsBuilder->build($paramsConfig, $this->cliArguments)->lock();
        } catch (\Exception $exception) {
            $msg = $exception->getMessage();

            if (stripos($msg, 'Incorrect spec string') !== false) {
                // TODO: throw better exception
                throw new \Exception('Cannot create parameter without short or long options');
            }

            $params = CliParams::create([
                'message'   => $msg,
                'params'    => $paramsConfig,
                'allCommands'  => $allCommands,
                'triedCommand' => $originalCmd,
            ])->lock();

            $handler->showHelp($params, $this->output);

            return;
        }

        $handler->handleCli($params, $this->output);
    }

    private function showHelp(Container $container, array $allCommands): void
    {
        $command     = $this->cliArguments[2] ?? '';
        $originalCmd = $command;
        $cmdOptions  = $allCommands[$command] ?? null;

        // General Help
        if ($command === '' || $cmdOptions === null) {
            $command     = self::KEY_HELP_AND_COMMAND_NOT_FOUND;
            $cmdOptions  = $allCommands[$command] ?? [];
            unset($allCommands[self::KEY_HELP_AND_COMMAND_NOT_FOUND]);
        }

        $handlerClass = $cmdOptions['handler'] ?? '';
        $paramsConfig = $cmdOptions['params'] ?? [];
        $params = CliParams::create([
            'args'        => array_slice($this->cliArguments, 2),
            'params'      => $paramsConfig,
            'allCommands' => $allCommands,
            'triedCommand' => $originalCmd,
        ])->lock();

        $handler = $this->getCommandHandler($handlerClass, $container, $command);

        if ($command === self::KEY_HELP_AND_COMMAND_NOT_FOUND) {
            $handler->handleCli($params, $this->output);
            return;
        }

        $handler->showHelp($params, $this->output);
    }

    private function printRemainingOutput(): void
    {
        foreach ($this->output->getStoredMessages() as $message) {
            echo $message, PHP_EOL;
        }
    }

    private function getCommandHandler(
        string $handlerClass,
        ContainerInterface $container,
        string $command
    ): CliControllerInterface {
        if ($handlerClass === '') {
            // TODO: throw better exception
            throw new \Exception(sprintf(
                'No Handler for command "%s"',
                $command
            ));
        }

        if (!is_subclass_of($handlerClass, CliControllerInterface::class, true)) {
            // TODO: throw better exception
            throw new \Exception(sprintf(
                'Controller must implement "%s", "%s" does not',
                AbstractCliController::class,
                $handlerClass
            ));
        }

        return $container->get($handlerClass);
    }

    private function handleCommandNotFound(
        string $triedCommand,
        array $allCommands,
        CliControllerInterface $controller
    ): void {
        unset($allCommands[self::KEY_HELP_AND_COMMAND_NOT_FOUND]);
        $params = CliParams::create([
            'triedCommand' => $triedCommand,
            'allCommands'  => $allCommands,
        ])->lock();

        $controller->handleCli($params, $this->output);
    }
}
