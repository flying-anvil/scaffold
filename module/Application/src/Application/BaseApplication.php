<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Application;

use FlyingAnvil\Scaffold\Application\Collection\ConfigCollection;
use DI\Container;
use DI\ContainerBuilder;

abstract class BaseApplication
{
    abstract public function run();

    /**
     * @param string[] $moduleNames
     * @param ConfigCollection $config
     * @throws \Exception
     */
    protected function applyModuleConfig(array $moduleNames, ConfigCollection $config): void
    {
        foreach ($moduleNames as $moduleName) {
            if (!is_subclass_of($moduleName, ModuleInterface::class, true)) {
                // TODO: throw better exception
                throw new \Exception(sprintf(
                    '"%s" is not a module. It must implement "%s"',
                    $moduleName,
                    ModuleInterface::class,
                ));
            }

            $moduleConfig = $moduleName::getConfig();
            $config->applyModuleConfig($moduleConfig);
        }
    }

    protected function initModules(ConfigCollection $config, Container $container): void
    {
        foreach ($config->get(ConfigCollection::KEY_MODULES, []) as $moduleName) {
            /** @var ModuleInterface $module */
            $module = $container->get($moduleName);
            $module->init();
        }
    }

    protected function buildDependencyContainer(ConfigCollection $config): Container
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions($config->get(ConfigCollection::KEY_DEPENDENCIES));
        $containerBuilder->useAutowiring(true);
        return $containerBuilder->build();
    }
}
