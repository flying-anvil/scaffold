<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application;

use FlyingAnvil\Scaffold\Application\Application\ModuleInterface;

class Module implements ModuleInterface
{
    public static function getConfig(): array
    {
        return require __DIR__ . '/../config/config.php';
    }

    public function init(): void
    {
    }
}
