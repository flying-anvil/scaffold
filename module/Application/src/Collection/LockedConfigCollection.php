<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Collection;

final class LockedConfigCollection extends ConfigCollection
{
    protected function __construct(array $elements)
    {
        parent::__construct();

        foreach ($elements as $key => $element) {
            $this->elements[$key] = $element;
        }
    }

    public static function create(array $elements = [])
    {
        return new self($elements);
    }

    public function add($element, string $key = ''): self
    {
        return $this;
    }

    public function merge(string $key, array ...$newConfigs): self
    {
        return $this;
    }

    public function applyModuleConfig(array $moduleConfig): self
    {
        return $this;
    }

    public function isLocked(): bool
    {
        return true;
    }

    public function lock(): self
    {
        return $this;
    }
}
