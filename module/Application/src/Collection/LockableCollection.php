<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Collection;

interface LockableCollection
{
    public function lock();

    public function isLocked(): bool;
}
