<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Collection;

class GenericCollection extends AbstractCollection
{
    private $isLocked = false;

    public function add($element, string $key = '')
    {
        if ($this->isLocked) {
            return $this;
        }

        return parent::add($element, $key);
    }

    public function set($element, string $key)
    {
        if ($this->isLocked) {
            return $this;
        }

        return parent::set($element, $key);
    }

    public function addMultiple(iterable $elements, bool $preserveKey = true)
    {
        if ($this->isLocked) {
            return $this;
        }

        return parent::addMultiple($elements, $preserveKey);
    }

    public function setMultiple(iterable $elements)
    {
        if ($this->isLocked) {
            return $this;
        }

        return parent::setMultiple($elements);
    }

    public function toArray(): array
    {
        return $this->elements;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function lock(): self
    {
        $this->isLocked = true;
        return $this;
    }

    public function isLocked(): bool
    {
        return $this->isLocked;
    }
}
