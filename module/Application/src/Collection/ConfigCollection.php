<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Collection;

use Exception;

class ConfigCollection extends AbstractCollection implements LockableCollection
{
    public const KEY_MODULES      = 'modules';
    public const KEY_DEPENDENCIES = 'dependencies';
    public const KEY_ROUTES       = 'routes';
    public const KEY_PROCESSORS   = 'processors';
    public const KEY_CLI          = 'cli';
    public const KEY_SOCKETS      = 'sockets';

    public function add($element, string $key = '')
    {
        if ($key === '') {
            // TODO: throw better exception
            throw new Exception(sprintf('Cannot add config for empty key'));
        }

        if (!is_array($element)) {
            // TODO: throw better exception
            throw new Exception(sprintf('Config must be an array'));
        }

        if ($key === 'modules' && isset($this->elements['module'])) {
            // TODO: throw better exception
            throw new Exception('cannot add modules afterwards');
        }

        $this->elements[$key] = $element;
        return $this;
    }

    public function merge(string $key, array ...$newConfigs)
    {
        $this->elements[$key] = array_replace_recursive($this->elements[$key] ?? [], ...$newConfigs);
        return $this;
    }

    public function applyModuleConfig(array $moduleConfig)
    {
        if (isset($moduleConfig['modules'])) {
            // TODO: throw better exception
            throw new Exception('cannot add modules afterwards');
        }

        $this->elements = array_replace_recursive($moduleConfig, $this->elements);
        return $this;
    }

    public function toArray(): array
    {
        return $this->elements;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function lock(): LockedConfigCollection
    {
        return LockedConfigCollection::create($this->elements);
    }

    public function isLocked(): bool
    {
        return false;
    }
}
