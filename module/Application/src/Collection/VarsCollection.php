<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Collection;

class VarsCollection extends AbstractCollection implements LockableCollection
{
    public function toArray(): array
    {
        return $this->elements;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function lock(): LockedVarsCollection
    {
        return LockedVarsCollection::create($this->elements);
    }

    public function isLocked(): bool
    {
        return false;
    }
}
