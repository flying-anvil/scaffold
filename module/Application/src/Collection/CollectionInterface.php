<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Collection;

use Countable;
use IteratorAggregate;
use JsonSerializable;

interface CollectionInterface extends JsonSerializable, IteratorAggregate, Countable
{
    public function add($element, string $key = '');

    public function set($element, string $key);

    public function addMultiple(iterable $elements, bool $preserveKey = true);

    public function setMultiple(iterable $elements);

    public function get(string $key, $defaultValue = null, bool $nullIsDefault = false);

    public function has(string $key): bool;

    public function toArray(): array;
}
