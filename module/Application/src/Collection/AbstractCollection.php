<?php

namespace FlyingAnvil\Scaffold\Application\Collection;

use Exception;
use Generator;

abstract class AbstractCollection implements CollectionInterface
{
    /** @var array */
    protected $elements = [];

    protected function __construct()
    {
    }

    public static function create(array $elements = [])
    {
        $collection = new static();
        $collection->addMultiple($elements);

        return $collection;
    }

    public function add($element, string $key = '')
    {
        if ($key === '') {
            $this->elements[] = $element;
            return $this;
        }

        if (isset($this->elements[$key])) {
            // TODO: throw better exception
            throw new \Exception(sprintf(
                'Key "%s" is already set',
                $key
            ));
        }

        $this->elements[$key] = $element;
        return $this;
    }

    public function set($element, string $key)
    {
        $this->elements[$key] = $element;
        return $this;
    }

    public function addMultiple(iterable $elements, bool $preserveKey = true)
    {
        foreach ($elements as $key => $element) {
            $this->add($element, $preserveKey ? $key : '');
        }

        return $this;
    }

    public function setMultiple(iterable $elements)
    {
        foreach ($elements as $key => $element) {
            $this->set($element, $key);
        }

        return $this;
    }

    public function get(string $key, $defaultValue = null, bool $nullIsDefault = false)
    {
        if (!isset($this->elements[$key])) {
            if ($defaultValue !== null || $nullIsDefault) {
                return $defaultValue;
            }

            // TODO: throw better exception
            throw new Exception(sprintf(
                'Cannot get "%s", does not exist',
                $key
            ));
        }

        return $this->elements[$key];
    }

    public function has(string $key): bool
    {
        return array_key_exists($key, $this->elements);
    }

    public function getIterator(): Generator
    {
        yield from $this->elements;
    }

    public function count(): int
    {
        return count($this->elements);
    }
}
