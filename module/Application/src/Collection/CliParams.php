<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Collection;

class CliParams extends AbstractCollection implements LockableCollection
{
    public const KEY_SHORT       = 'short';
    public const KEY_LONG        = 'long';
    public const KEY_MODIFIER    = 'modifier';
    public const KEY_DESCRIPTION = 'description'; // currently unused
    public const KEY_DEFAULT     = 'default';

    public const MOD_REQUIRED    = ':';
    public const MOD_MULTIVALUE  = '+';
    public const MOD_OPTIONAL    = '?';
    public const MOD_FLAG        = '';
    public const MOD_INCREMENTAL = 'incremental';

    public const MOD_MAPPING = [
        self::MOD_REQUIRED    => 'required',
        self::MOD_MULTIVALUE  => 'multivalue',
        self::MOD_OPTIONAL    => 'optional',
        self::MOD_FLAG        => 'flag',
        self::MOD_INCREMENTAL => 'incremental',
    ];

    private $isLocked = false;

    public function add($element, string $key = '')
    {
        if ($this->isLocked) {
            return $this;
        }

        return parent::add($element, $key);
    }

    public function set($element, string $key)
    {
        if ($this->isLocked) {
            return $this;
        }

        return parent::set($element, $key);
    }

    public function addMultiple(iterable $elements, bool $preserveKey = true)
    {
        if ($this->isLocked) {
            return $this;
        }

        return parent::addMultiple($elements, $preserveKey);
    }

    public function setMultiple(iterable $elements)
    {
        if ($this->isLocked) {
            return $this;
        }

        return parent::setMultiple($elements);
    }

    public function toArray(): array
    {
        return $this->elements;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function lock(): self
    {
        $this->isLocked = true;
        return $this;
    }

    public function isLocked(): bool
    {
        return $this->isLocked;
    }
}
