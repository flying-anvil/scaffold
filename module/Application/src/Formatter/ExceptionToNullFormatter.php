<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Formatter;

use Exception;
use JsonSerializable;

final class ExceptionToNullFormatter implements JsonSerializable
{
    /** @var mixed | null */
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public static function create(object $object, string $method): ExceptionToNullFormatter
    {
        if (method_exists($object, $method)) {
            try {
                return new self($object->$method());
            } catch (Exception $exception) {
                return new self(null);
            }
        }

        return new self(null);
    }

    public function getValue()
    {
        return $this->value;
    }

    public function jsonSerialize()
    {
        return $this->value;
    }
}
