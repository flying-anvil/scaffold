<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Router;

use FlyingAnvil\Scaffold\Application\Application\ExtendedHttpRequest;
use FlyingAnvil\Scaffold\Application\Application\ExtendedHttpResponse;
use FlyingAnvil\Scaffold\Application\Controller\ControllerInterface;
use FlyingAnvil\Scaffold\Application\Processor\ErrorProcessorInterface;
use FlyingAnvil\Scaffold\Application\Processor\PostProcessInput;
use FlyingAnvil\Scaffold\Application\Processor\PostProcessorInterface;
use FlyingAnvil\Scaffold\Application\Processor\PreProcessInput;
use FlyingAnvil\Scaffold\Application\Processor\PreProcessorInterface;
use FlyingAnvil\Scaffold\Application\Processor\PreProcessResult;
use FlyingAnvil\Scaffold\Application\Collection\ConfigCollection;
use FlyingAnvil\Scaffold\Application\Collection\LockedVarsCollection;
use FlyingAnvil\Scaffold\Application\Controller\AbstractController;
use FlyingAnvil\Scaffold\Application\Exception\UnbuiltRouteException;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;
use FlyingAnvil\PhpRenderer\Renderer\Renderer;
use Http\MissingRequestMetaVariableException;
use Psr\Container\ContainerInterface;

class RouteDispatcher
{
    /** @var ConfigCollection */
    private $config;

    /** @var Route[] */
    private $routes;

    /** @var Dispatcher */
    private $dispatcher;

    public function __construct(ConfigCollection $config)
    {
        $this->config = $config;
    }

    public function addRoute(Route $route): self
    {
        $this->routes[] = $route;
        return $this;
    }

    public function build(): self
    {
        $this->dispatcher = simpleDispatcher(function (RouteCollector $routeCollector) {
            foreach ($this->routes as $route) {
                $routeCollector->addRoute(
                    $route->getMethod(),
                    $route->getRoute(),
                    $route
                );
            }
        });

        return $this;
    }

    /**
     * @param ContainerInterface $container
     * @param ExtendedHttpRequest $request
     * @param ExtendedHttpResponse $response
     * @throws MissingRequestMetaVariableException
     * @throws UnbuiltRouteException
     */
    public function dispatch(ContainerInterface $container, ExtendedHttpRequest $request, ExtendedHttpResponse $response): void
    {
        if (!$this->dispatcher) {
            throw new UnbuiltRouteException('Routes are not built yet, please run build() before dispatching');
        }

        $routeInfo = $this->dispatcher->dispatch(
            $request->getMethod(),
            '/' . urldecode(trim($request->getPath(), '/'))
        );

        $foundStatus = $routeInfo[0];

        switch ($foundStatus) {
            case Dispatcher::FOUND:
                /** @var Route $route */
                [,$route, $vars]  = $routeInfo;
                $handlerClass     = $route->getHandlerClass();
                $vars             = LockedVarsCollection::create($vars);
                $input            = PreProcessInput::create($request, $route, $vars);
                $preprocessResult = $this->preProcess($container, $input);

                $overrideController = $preprocessResult->getOverrideController();
                if ($overrideController !== null) {
                    $handlerClass = $overrideController;
                }

                $this->callController($handlerClass, $container, $request, $response, $vars);
                $this->postProcess($container, $request, $response);

                break;
            case Dispatcher::NOT_FOUND:
                $this->processError($container, $request, $response, ErrorProcessorInterface::KEY_404);
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                $this->processError($container, $request, $response, ErrorProcessorInterface::KEY_405);
                break;
        }
    }

    private function preProcess(ContainerInterface $container, PreProcessInput $input): PreProcessResult
    {
        $result     = PreProcessResult::create();
        $processors = $this->config->get(ConfigCollection::KEY_PROCESSORS, ['pre' => []]);

        foreach ($processors['pre'] as $preprocessorName) {
            if (!is_subclass_of($preprocessorName, PreProcessorInterface::class)) {
                // TODO: throw better exception
                throw new \Exception(sprintf(
                    'Preprocessor must implement "%s", "%s" does not',
                    PreProcessorInterface::class,
                    $preprocessorName
                ));
            }

            /** @var PreProcessorInterface $instance */
            $instance = $container->get($preprocessorName);
            $instance->preProcess($input, $result);

            if ($result->preprocessingIsStopped()) {
                break;
            }
        }

        return $result;
    }

    private function postProcess(
        ContainerInterface $container,
        ExtendedHttpRequest $request,
        ExtendedHttpResponse $response
    ): void {
        $processors = $this->config->get(ConfigCollection::KEY_PROCESSORS, ['post' => []]);

        foreach ($processors['post'] as $postprocessorsName) {
            if (!is_subclass_of($postprocessorsName, PostProcessorInterface::class)) {
                // TODO: throw better exception
                throw new \Exception(sprintf(
                    'PostProcessor must implement "%s", "%s" does not',
                    PreProcessorInterface::class,
                    $postprocessorsName
                ));
            }

            /** @var PostProcessorInterface $instance */
            $instance = $container->get($postprocessorsName);
            $instance->postProcess(PostProcessInput::create($request, $response));
        }
    }

    private function callController(
        string $handlerClass,
        ContainerInterface $container,
        ExtendedHttpRequest $request,
        ExtendedHttpResponse $response,
        LockedVarsCollection $vars
    ): void {
        if (!is_subclass_of($handlerClass, ControllerInterface::class, true)) {
            // TODO: throw better exception
            throw new \Exception(sprintf(
                'Controller must implement "%s", "%s" does not',
                ControllerInterface::class,
                $handlerClass
            ));
        }

        /** @var ControllerInterface $handler */
        $handler  = $container->get($handlerClass);
        $handler->prepare($request, $response, $container->get(Renderer::class));
        $handler->handle($vars);
    }

    private function processError(
        ContainerInterface $container,
        ExtendedHttpRequest $request,
        ExtendedHttpResponse $response,
        string $statusCode
    ): void {
        $processors      = $this->config->get(ConfigCollection::KEY_PROCESSORS);
        $errorProcessors = $processors[ErrorProcessorInterface::KEY_ERROR];
        $notFoundHandler = $errorProcessors[$statusCode];

        if (!is_subclass_of($notFoundHandler, PostProcessorInterface::class)) {
            // TODO: throw better exception
            throw new \Exception(sprintf(
                'ErrorProcessor must implement "%s", "%s" does not',
                ErrorProcessorInterface::class,
                $notFoundHandler
            ));
        }

        $response->setStatusCode($statusCode);
        $input = PostProcessInput::create($request, $response);

        /** @var ErrorProcessorInterface $processor */
        $processor  = $container->get($notFoundHandler);
        $processor->postProcess($input);
    }
}
