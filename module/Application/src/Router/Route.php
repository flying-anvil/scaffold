<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Router;

final class Route
{
    /** @var string */
    private $method;

    /** @var string */
    private $route;

    /** @var string */
    private $handlerClass;

    /** @var string */
    private $routeName;

    private function __construct(string $method, string $route, string $handlerClass, string $routeName)
    {
        $this->method         = $method;
        $this->route          = $route;
        $this->handlerClass   = $handlerClass;
        $this->routeName      = $routeName;
    }

    public static function create(string $method, string $route, string $handler, string $routeName = 'unnamed'): self
    {
        $route = '/' . ltrim($route, '/');
        $name  = ltrim($routeName, '/');

        return new self($method, $route, $handler, $name);
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getRoute(): string
    {
        return $this->route;
    }

    public function getHandlerClass(): string
    {
        return $this->handlerClass;
    }

    public function getRouteName(): string
    {
        return $this->routeName;
    }
}
