<?php

/** @noinspection AdditionOperationOnArraysInspection */

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Router;

class RouteConfigToArray
{
    /**
     * @param array $config
     * @return Route[]
     */
    public function convert(array $config): array
    {
//        var_dump($this->buildRoutes($config));
//        die;
        return $this->buildRoutes($config);
    }

    private function buildRoutes(array $routeArray, string $routeNamePrefix = ''): array
    {
        $routes = [];

        foreach ($routeArray as $routeName => $rawRoute) {
            $routeName = $routeNamePrefix . $routeName;

            if (is_array($rawRoute[key($rawRoute)])) {
                $routes1 = $this->buildRoutes($rawRoute, $routeName . '/');
                $routes += $routes1;
                continue;
            }

            $routes[$routeName] = Route::create(
                $rawRoute['method'],
                $this->helpWithRouteNaming($routeName, $rawRoute['route'] ?? ''),
                $rawRoute['handler'],
                $this->helpWithRouteNaming($routeName, $rawRoute['name'] ?? ''),
            );
        }

        return $routes;
    }

    private function helpWithRouteNaming(string $routeName, string $route): string
    {
        if ($route === '') {
            return $routeName;
        }

        if ($route[0] !== '/') {
            if (strpos($routeName, '/') === false) {
                return $route;
            }

            return substr($routeName, 0, strrpos($routeName, '/')) . '/' . $route;
        }

//        if ($route[0] !== '.') {
            return $route;
//        }

        // TODO: a dot (.) could be used to append without overriding
    }
}
