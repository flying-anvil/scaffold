<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Exception;

use Exception;

class UnbuiltRouteException extends Exception
{
}
