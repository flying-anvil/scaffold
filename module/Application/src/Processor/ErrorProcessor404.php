<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Processor;

class ErrorProcessor404 implements ErrorProcessorInterface
{
    public function postProcess(PostProcessInput $input): void
    {
        $input->getResponse()->json([
            'statucCode' => 404,
            'messages' => [
                'page not found',
                microtime(true),
            ]
        ]);
    }
}
