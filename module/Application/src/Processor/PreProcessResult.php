<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Processor;

final class PreProcessResult
{
    /** @var bool */
    private $stopPreprocessing = false;

    /** @var string */
    private $overrideController;

    private function __construct()
    {
    }

    public static function create(): PreProcessResult
    {
        return new self();
    }

    public function stopPreprocessing(bool $stopPreprocessing = true): PreProcessResult
    {
        $this->stopPreprocessing = $stopPreprocessing;
        return $this;
    }

    public function preprocessingIsStopped(): bool
    {
        return $this->stopPreprocessing;
    }

    public function overrideController(string $overrideController): PreProcessResult
    {
        $this->overrideController = $overrideController;
        return $this;
    }

    public function getOverrideController(): ?string
    {
        return $this->overrideController;
    }
}
