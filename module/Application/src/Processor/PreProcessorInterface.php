<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Processor;

interface PreProcessorInterface
{
    public function preProcess(PreProcessInput $input, PreProcessResult $result): void;
}
