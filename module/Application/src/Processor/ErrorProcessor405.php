<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Processor;

class ErrorProcessor405 implements ErrorProcessorInterface
{
    public function postProcess(PostProcessInput $input): void
    {
        $input->getResponse()->json([
            'statucCode' => 405,
            'messages' => [
                'method not allowed',
                'cannot use ' . $input->getRequest()->getMethod()
            ]
        ]);
    }
}
