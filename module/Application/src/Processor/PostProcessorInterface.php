<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Processor;

interface PostProcessorInterface
{
    public function postProcess(PostProcessInput $input): void;
}
