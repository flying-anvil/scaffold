<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Processor;

interface ErrorProcessorInterface extends PostProcessorInterface
{
    public const KEY_ERROR = 'error';
    public const KEY_404   = '404';
    public const KEY_405   = '405';
}
