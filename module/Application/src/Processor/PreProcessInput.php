<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Processor;

use FlyingAnvil\Scaffold\Application\Collection\VarsCollection;
use FlyingAnvil\Scaffold\Application\Router\Route;
use Http\Request;

final class PreProcessInput
{
    /** @var Request */
    private $request;

    /** @var Route */
    private $route;

    /** @var VarsCollection */
    private $vars;

    private function __construct(Request $request, Route $route, VarsCollection $vars)
    {
        $this->request = $request;
        $this->route   = $route;
        $this->vars    = $vars;
    }

    public static function create(Request $request, Route $route, VarsCollection $vars): self
    {
        return new self($request, $route, $vars);
    }

    public function getRoute(): Route
    {
        return $this->route;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function getVars(): VarsCollection
    {
        return $this->vars;
    }
}
