<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Processor;

use FlyingAnvil\Scaffold\Application\Application\ExtendedHttpRequest;
use FlyingAnvil\Scaffold\Application\Application\ExtendedHttpResponse;

final class PostProcessInput
{
    /** @var ExtendedHttpRequest */
    private $request;

    /** @var ExtendedHttpResponse */
    private $response;

    private function __construct(ExtendedHttpRequest $request, ExtendedHttpResponse $response)
    {
        $this->request  = $request;
        $this->response = $response;
    }

    public static function create(ExtendedHttpRequest $request, ExtendedHttpResponse $response): self
    {
        return new self($request, $response);
    }

    public function getResponse(): ExtendedHttpResponse
    {
        return $this->response;
    }

    public function getRequest(): ExtendedHttpRequest
    {
        return $this->request;
    }
}
