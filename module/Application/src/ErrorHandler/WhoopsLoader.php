<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\ErrorHandler;

use Whoops\Handler\PrettyPageHandler;
use Whoops\Run as Whoops;

class WhoopsLoader
{
    public function load(): void
    {
        $whoops = new Whoops();
        if (APP_ENV !== 'production') {
            $whoops->appendHandler(new PrettyPageHandler);
        } /* else {
            $whoops->appendHandler(function ($e) {
                echo 'Todo: Friendly error page and send a message to the developer';
            });
        } */
        $whoops->register();
    }
}
