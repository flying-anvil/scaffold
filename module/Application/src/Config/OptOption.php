<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Config;

use GetOptionKit\Option;

class OptOption extends Option
{
    public function getValue()
    {
        if (null !== $this->value) {
            return $this->value;
        }

        return $this->getDefaultValue();
    }
}
