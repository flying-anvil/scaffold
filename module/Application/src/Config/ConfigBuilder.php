<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Config;

use FlyingAnvil\Scaffold\Application\Collection\ConfigCollection;
use function DI\get;
use DirectoryIterator;
use Exception;

class ConfigBuilder
{
    /**
     * @param string $configPath
     * @return ConfigCollection
     * @throws Exception
     */
    public function build(string $configPath = ROOT . '/config'): ConfigCollection
    {
        if (!is_dir($configPath)) {
            // TODO: throw better exception
            throw new Exception(sprintf(
                'Config directory "%s" does not exist',
                $configPath,
            ));
        }

        $config   = ConfigCollection::create();
        $iterator = new DirectoryIterator($configPath);

        foreach ($iterator as $entry) {
            if ($entry->isDot()) {
                continue;
            }

            $entryName       = $entry->getPathname();
            $key             = $entry->getFilename();
            $globalName      = $entryName . '/global.php';
            $environmentName = $entryName . '/' . APP_ENV . '.php';

            $global      = [];
            $environment = [];

            // Get File Config
            if ($entry->getExtension() === 'php') {
                $key        = substr($entry->getFilename(), 0, -4);
                $fileConfig = require $entry->getPathname();

                $config->merge($key, $fileConfig);

                continue;
            }

            // Get Directory Config
            if (is_dir($entryName)) {
                if (file_exists($globalName)) {
                    $global = require $globalName;
                }

                if (file_exists($environmentName)) {
                    $environment = require $environmentName;
                }
            }

            $config->merge($key, $global, $environment);
        }

        // inject config into dependencies
        $config->merge('dependencies', [
            ConfigCollection::class => $config,
            'config'                => get(ConfigCollection::class)
        ]);

        return $config;
    }
}
