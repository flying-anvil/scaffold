<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Config;

use FlyingAnvil\Scaffold\Application\Collection\CliParams;
use GetOptionKit\Exception\InvalidOptionException;
use GetOptionKit\OptionCollection;
use GetOptionKit\OptionParser;

class CliParamsBuilder
{
    /**
     * @param array $configParams
     * @param array $cliArguments
     * @return CliParams
     * @throws \Exception
     * @throws InvalidOptionException
     */
    public function build(array $configParams, array $cliArguments): CliParams
    {
        $params = CliParams::create();
        $specs  = new OptionCollection();

        foreach ($configParams as $name => $config) {
            $short       = $config[CliParams::KEY_SHORT] ?? '';
            $long        = $config[CliParams::KEY_LONG] ?? '';
            $modifier    = $config[CliParams::KEY_MODIFIER] ?? '';
            $description = $config[CliParams::KEY_DESCRIPTION] ?? '';

            $shortLong = [];
            if ($short) {
                $shortLong[] = $short;
            }

            if ($long) {
                $shortLong[] = $long;
            }

            $increment = false;
            if ($modifier === CliParams::MOD_INCREMENTAL) {
                $increment = true;
                $modifier = '';
            }

            $current   = (implode('|', $shortLong) . $modifier);
            $option    = (new OptOption($current))->valueName($name);
            $option->desc($description ?? '');

            if ($increment) {
                $option->incremental();
            }

            $specs->add($option);
        }

        $optionParser = new OptionParser($specs);
        $result       = $optionParser->parse($cliArguments);

        foreach ($configParams as $name => $config) {
            $param = $result->get($name);
            if ($param === null
                && ($config[CliParams::KEY_MODIFIER] ?? '') === CliParams::MOD_REQUIRED
                && ($config[CliParams::KEY_DEFAULT] ?? '') === ''
            ) {
                // TODO: throw better exception
                throw new \Exception(sprintf(
                    'Missing required param "%s"',
                    $name
                ));
            }

            $params->add($param ?? $config[CliParams::KEY_DEFAULT] ?? null, $name);
        }

        return $params;
    }
}
