<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Controller;

use FlyingAnvil\Scaffold\Application\Collection\VarsCollection;

class IndexController extends AbstractController
{
    public function handle(VarsCollection $vars): void
    {
        $this->getResponse()->setContent('It Works (TODO)');
    }
}
