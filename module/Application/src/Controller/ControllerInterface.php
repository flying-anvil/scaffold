<?php declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Controller;

use FlyingAnvil\PhpRenderer\Renderer\Renderer;
use FlyingAnvil\Scaffold\Application\Application\ExtendedHttpRequest;
use FlyingAnvil\Scaffold\Application\Application\ExtendedHttpResponse;
use FlyingAnvil\Scaffold\Application\Collection\VarsCollection;

interface ControllerInterface
{
    public function prepare(
        ExtendedHttpRequest $request,
        ExtendedHttpResponse $response,
        Renderer $renderer
    ): void;

    public function handle(VarsCollection $vars): void;
}