<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Controller;

use FlyingAnvil\Scaffold\Application\Application\CliOutput;
use FlyingAnvil\Scaffold\Application\Collection\CliParams;

abstract class AbstractCliController implements CliControllerInterface
{
    abstract public function handleCli(CliParams $params, CliOutput $output): void;

    public function showHelp(CliParams $params, CliOutput $output): void
    {
        $this->showDefaultHelp($params, $output);
    }

    protected function showDefaultHelp(CliParams $params, CliOutput $output): void
    {
        $triedCommand = $params->get('triedCommand');
//        $msg          = $params->get('message', 'help for ' . $triedCommand);
        $cliParams    = $params->get('params');
        $description  = $params->get('allCommands')[$triedCommand]['description'] ?? 'No description :(';
        $maxLenght    = 0;

        if (!empty($cliParams)) {
            $maxLenght = max(array_map('strlen', array_keys($cliParams)));
        }

//        $output->writeLine('Message: ' . $msg . PHP_EOL);
        $output->writeLine();
        $output->writeLine('Command Description: ' . $description . PHP_EOL);

        if (empty($cliParams)) {
            return;
        }

        $output->writeUnderline('Usage with parameters:');

        foreach ($cliParams as $name => $cliParam) {
            $output->writeLine($this->formatParamUsage($name, $cliParam, $maxLenght));
        }

        $output->writeLine();
    }

    protected function formatParamUsage(string $name, array $param, int $maxLength): string
    {
        $modifier = $param['modifier'];
        $short    = isset($param['short']) ? '-' . $param['short'] . '  ' : '';
        $long     = isset($param['long']) ? '--' . $param['long'] : '';

        return sprintf(
            ' %s │ %s' . PHP_EOL . ' %s │ %s%s (%s)' . PHP_EOL . '%s',
            str_pad($name, $maxLength, ' '),
            $param['description'] ?? '',
            str_repeat('⎺', $maxLength),
            $short,
            $long,
            CliParams::MOD_MAPPING[$modifier],
            str_repeat(' ', $maxLength) . '  ╰',
        );
    }
}
