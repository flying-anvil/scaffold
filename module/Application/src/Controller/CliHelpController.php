<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Controller;

use FlyingAnvil\Scaffold\Application\Application\CliOutput;
use FlyingAnvil\Scaffold\Application\Collection\CliParams;

class CliHelpController extends AbstractCliController
{
    public function handleCli(CliParams $params, CliOutput $output): void
    {
        $triedCommand = $params->get('triedCommand', '');
        $allCommands  = $params->get('allCommands', []);

        if ($triedCommand !== '') {
            $output->writeLine(sprintf(
                'Command "%s" does not exist',
                $triedCommand
            ));
        }

        $this->printCommands($output, $allCommands);
    }

    public function showHelp(CliParams $params, CliOutput $output): void
    {
        $allCommands = $params->get('allCommands', []);

        $this->printCommands($output, $allCommands);
    }

    private function printCommands(CliOutput $output, $allCommands): void
    {
        $output->writeLine();
        $output->writeUnderline('Available commands:');

        $maxLength = max(array_map('strlen', array_keys($allCommands)));

        foreach ($allCommands as $name => $options) {
            $padded       = str_pad($name, $maxLength, ' ');
            $nameLength   = strlen($name);
            $description  = $options['description'] ?? '';
            $params       = $options['params'] ?? [];
            $paramsString = '-none-';

            if (!empty($params)) {
                $paramsString = '"' . implode('", "', array_keys($params)) . '"';
            }

            $output->writeLine(
                sprintf(
                    ' %s ┊ %s' . PHP_EOL . ' %s ┊ params: %s' . PHP_EOL . '%s',
                    $padded,
                    $description,
                    str_repeat('⎺', $nameLength) . str_repeat(' ', $maxLength - $nameLength),
                    $paramsString,
                    str_repeat(' ', $maxLength) . '  ╯',
                )
            );
        }

        $output->writeLine();
    }
}
