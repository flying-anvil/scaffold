<?php declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Controller;

use FlyingAnvil\Scaffold\Application\Application\CliOutput;
use FlyingAnvil\Scaffold\Application\Collection\CliParams;

interface CliControllerInterface
{
    public function handleCli(CliParams $params, CliOutput $output): void;

    public function showHelp(CliParams $params, CliOutput $output): void;
}