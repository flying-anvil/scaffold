<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Controller;

use FlyingAnvil\Scaffold\Application\Application\CliOutput;
use FlyingAnvil\Scaffold\Application\Collection\CliParams;

class TestCliController extends AbstractCliController
{
    public function handleCli(CliParams $params, CliOutput $output): void
    {
        foreach ($params as $name => $param) {
            $output->writeLine($name . ': ' . $param);
        }

        $output->storeMessage('Number of params: ' . count($params));
    }
}
