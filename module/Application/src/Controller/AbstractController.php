<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Application\Controller;

use FlyingAnvil\PhpRenderer\Renderer\Renderer;
use FlyingAnvil\Scaffold\Application\Application\ExtendedHttpRequest;
use FlyingAnvil\Scaffold\Application\Application\ExtendedHttpResponse;
use FlyingAnvil\Scaffold\Application\Collection\VarsCollection;

abstract class AbstractController implements ControllerInterface
{
    /** @var ExtendedHttpRequest */
    private $request;

    /** @var ExtendedHttpResponse */
    private $response;

    /** @var Renderer */
    private $renderer;

    /**
     * @param ExtendedHttpRequest $request
     * @param ExtendedHttpResponse $response
     * @param Renderer $renderer
     */
    public function prepare(
        ExtendedHttpRequest $request,
        ExtendedHttpResponse $response,
        Renderer $renderer
    ): void {
        $this->request  = $request;
        $this->response = $response;
        $this->renderer = $renderer;
    }

    abstract public function handle(VarsCollection $vars): void;

    protected function jsonResponse(array $data, int $statusCode = 200): void
    {
        $this->response->json($data);
        $this->response->setStatusCode($statusCode);
    }

    protected function redirect(string $url, int $statusCode = 303): void
    {
        $this->response->setHeader('Location', $url);
        $this->response->setStatusCode($statusCode);
    }

    protected function getRequest(): ExtendedHttpRequest
    {
        return $this->request;
    }

    protected function getResponse(): ExtendedHttpResponse
    {
        return $this->response;
    }

    protected function getRenderer(): Renderer
    {
        return $this->renderer;
    }
}
