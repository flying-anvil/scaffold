<?php

use FlyingAnvil\Scaffold\Application\Application\Application;
use FlyingAnvil\Scaffold\Application\Application\CliApplication;

require_once __DIR__ . '/../vendor/autoload.php';

if (!defined('APP_ENV')) {
    $env = file_exists(__DIR__ . '/../env')
        ? file_get_contents(__DIR__ . '/../env')
        : 'testing';
    define('APP_ENV', $env);
}

!defined('ROOT') && define('ROOT', dirname(__DIR__));

if (strpos(PHP_SAPI, 'fpm') !== false) {
    $start = microtime(true);

    $application = new Application($_GET, $_POST, $_COOKIE, $_FILES, $_SERVER);
    $application->run();

    $end = microtime(true);
//    echo 'Time: ' . ($end - $start) . 's', PHP_EOL;
    return;
}

if (strpos(PHP_SAPI, 'cli') !== false) {
    $start = microtime(true);

    $application = new CliApplication($argv);
    $application->run();

    $end = microtime(true);
//    echo 'Time: ' . ($end - $start) . 's', PHP_EOL;
    return;
}
