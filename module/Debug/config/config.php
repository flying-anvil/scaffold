<?php

namespace FlyingAnvil\Scaffold\Debug;

use FlyingAnvil\Scaffold\Application\Collection\CliParams;
use FlyingAnvil\Scaffold\Application\Controller\TestCliController;
use FlyingAnvil\Scaffold\Debug\Controller\ConfigDebugController;
use FlyingAnvil\Scaffold\Debug\Controller\RequestDebugController;
use FlyingAnvil\Scaffold\Debug\Controller\RouteDebugController;
use FlyingAnvil\Scaffold\Debug\SocketController\ReturnMessageToSenderAndConsoleSocket;
use FlyingAnvil\Scaffold\WebSocket\Options\SocketOptions;

return [
    'routes' => [
        'debug' => [
            'request[/{format}]' => [
                'method'  => 'GET',
                'handler' => RequestDebugController::class,
                'name'    => '/debug/request'
            ],
            'config[/{format}]' => [
                'method'  => 'GET',
                'handler' => ConfigDebugController::class,
                'name'    => '/debug/config',
            ],
            'routes[/{format}]' => [
                'method'  => 'GET',
                'handler' => RouteDebugController::class,
                'name'    => '/debug/routes'
            ],
            'route-params' => [
                '{name}' => [
                    'method'  => 'GET',
                    'handler' => RequestDebugController::class,
                    'name'    => '/debug/route-params'
                ]
            ],
        ],
    ],
    'cli' => [
        'test' => [
            'description' => 'Dummy command for testing purposes, not doing anything',
            'handler'     => TestCliController::class,
            'params'      => [
                'date' => [
                    CliParams::KEY_SHORT       => 'd',
                    CliParams::KEY_LONG        => 'date',
                    CliParams::KEY_MODIFIER    => CliParams::MOD_REQUIRED,
                    CliParams::KEY_DESCRIPTION => 'Some random date which is ignored' .
                        ' (Does not need to be a date either)',
                    CliParams::KEY_DEFAULT     => null,
                ],
                'file' => [
                    CliParams::KEY_SHORT       => 'f',
                    CliParams::KEY_LONG        => 'file',
                    CliParams::KEY_MODIFIER    => CliParams::MOD_OPTIONAL,
                    CliParams::KEY_DESCRIPTION => 'File which is also ignored',
                    CliParams::KEY_DEFAULT     => '',
                ],
            ]
        ],
    ],
    'sockets' => [
        SocketOptions::KEY_ROUTES      => [
            'debug/return' => [
                'handler' => ReturnMessageToSenderAndConsoleSocket::class,
            ]
        ],
    ],
];
