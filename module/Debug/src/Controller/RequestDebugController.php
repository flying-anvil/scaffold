<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Debug\Controller;

use FlyingAnvil\Scaffold\Application\Collection\VarsCollection;
use FlyingAnvil\Scaffold\Application\Controller\AbstractController;
use FlyingAnvil\Scaffold\Application\Formatter\ExceptionToNullFormatter;

class RequestDebugController extends AbstractController
{
    public function handle(VarsCollection $vars): void
    {
        $format = $vars->get('format', 'json');

        switch ($format) {
            case 'json':
                $this->formatJson($vars->toArray());
                return;
            case 'plain':
                $this->formatPlain($vars->toArray());
                return;
            case 'vardump':
                $this->formatVardump($vars->toArray());
                return;
            default:
                $this->formatJson($vars->toArray());
                return;
        }
    }

    private function formatJson(array $vars): void
    {
        $request = $this->getRequest();

        $this->jsonResponse([
            'controller' => [
                'class' => self::class,
                'vars'  => $vars,
            ],
            'request' => [
                'method'          => ExceptionToNullFormatter::create($request, 'getMethod')->getValue(),
                'httpAccept'      => ExceptionToNullFormatter::create($request, 'getHttpAccept')->getValue(),
                'ipAddress'       => ExceptionToNullFormatter::create($request, 'getIpAddress')->getValue(),
                'referer'         => ExceptionToNullFormatter::create($request, 'getReferer')->getValue(),
                'userAgent'       => ExceptionToNullFormatter::create($request, 'getUserAgent')->getValue(),
                'uri'             => ExceptionToNullFormatter::create($request, 'getUri')->getValue(),
                'headers'         => $request->getHeaders(),
                'path'            => $request->getPath(),
                'cookies'         => $request->getCookies(),
                'files'           => $request->getFiles(),
                'parameters'      => $request->getParameters(),
                'queryParameters' => $request->getQueryParameters(),
                'bodyParameters'  => $request->getBodyParameters(),
                'rawBody'         => $request->getRawBody(),
            ]
        ]);
    }

    private function formatPlain(array $vars): void
    {
        $request = $this->getRequest();

        $this->getResponse()->setContent(nl2br(str_replace(' ', '&nbsp', htmlentities(print_r([
            'controller' => [
                'class' => self::class,
                'vars'  => $vars,
            ],
            'request' => [
                'method'          => ExceptionToNullFormatter::create($request, 'getMethod')->getValue(),
                'httpAccept'      => ExceptionToNullFormatter::create($request, 'getHttpAccept')->getValue(),
                'ipAddress'       => ExceptionToNullFormatter::create($request, 'getIpAddress')->getValue(),
                'referer'         => ExceptionToNullFormatter::create($request, 'getReferer')->getValue(),
                'userAgent'       => ExceptionToNullFormatter::create($request, 'getUserAgent')->getValue(),
                'uri'             => ExceptionToNullFormatter::create($request, 'getUri')->getValue(),
                'path'            => $request->getPath(),
                'body'            => $request->getBodyParameters(),
                'cookies'         => $request->getCookies(),
                'files'           => $request->getFiles(),
                'parameters'      => $request->getParameters(),
                'queryParameters' => $request->getQueryParameters(),
                'bodyParameters'  => $request->getBodyParameters(),
                'rawBody'         => $request->getRawBody(),
            ]
        ], true)))));
    }

    private function formatVardump(array $vars): void
    {
        $request = $this->getRequest();

        $oldDepth    = ini_set('xdebug.var_display_max_depth', '-1');
        $oldChildren = ini_set('xdebug.var_display_max_children', '-1');
        $oldData     = ini_set('xdebug.var_display_max_data', '-1');

        ob_start();

        var_dump([
            'controller' => [
                'class' => self::class,
                'vars'  => $vars,
            ],
            'request' => [
                'method'          => ExceptionToNullFormatter::create($request, 'getMethod')->getValue(),
                'httpAccept'      => ExceptionToNullFormatter::create($request, 'getHttpAccept')->getValue(),
                'ipAddress'       => ExceptionToNullFormatter::create($request, 'getIpAddress')->getValue(),
                'referer'         => ExceptionToNullFormatter::create($request, 'getReferer')->getValue(),
                'userAgent'       => ExceptionToNullFormatter::create($request, 'getUserAgent')->getValue(),
                'uri'             => ExceptionToNullFormatter::create($request, 'getUri')->getValue(),
                'path'            => $request->getPath(),
                'body'            => $request->getBodyParameters(),
                'cookies'         => $request->getCookies(),
                'files'           => $request->getFiles(),
                'parameters'      => $request->getParameters(),
                'queryParameters' => $request->getQueryParameters(),
                'bodyParameters'  => $request->getBodyParameters(),
                'rawBody'         => $request->getRawBody(),
            ]
        ]);

        $varDump = ob_get_clean();

        ini_set('xdebug.var_display_max_depth', $oldDepth);
        ini_set('xdebug.var_display_max_children', $oldChildren);
        ini_set('xdebug.var_display_max_data', $oldData);

        $this->getResponse()->setContent($varDump);
    }
}
