<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Debug\Controller;

use FlyingAnvil\Scaffold\Application\Collection\ConfigCollection;
use DI\Definition\Reference;
use FlyingAnvil\Scaffold\Application\Collection\VarsCollection;
use FlyingAnvil\Scaffold\Application\Controller\AbstractController;

class ConfigDebugController extends AbstractController
{
    /** @var ConfigCollection */
    private $config;

    public function __construct(ConfigCollection $config)
    {
        $this->config = $config;
    }

    public function handle(VarsCollection $vars): void
    {
        $format = $vars->get('format', 'json');

        switch ($format) {
            case 'json':
                $this->formatJson();
                return;
            case 'plain':
                $this->formatPlain();
                return;
            case 'vardump':
                $this->formatVardump();
                return;
            default:
                $this->formatJson();
                return;
        }
    }

    private function purifyConfig(): array
    {
        $config            = $this->config->toArray();
        $debugDependencies = [];

        foreach ($this->config->get(ConfigCollection::KEY_DEPENDENCIES) as $key => $value) {
            $newValue = '';

            if (is_object($value)) {
                if ($value instanceof Reference) {
                    $newValue = 'Reference To: ' . $value->getTargetEntryName();
                } else {
                    $newValue = get_class($value);
                }
            }

            $debugDependencies[$key] = $newValue;
        }

        $config['dependencies'] = $debugDependencies;
        $config['dependencies']['NOTICE'] = 'Only class names and reference targets' .
            ' are shown due to possible recursion';

        return $config;
    }

    private function formatJson(): void
    {
        $this->jsonResponse($this->purifyConfig());
    }

    private function formatPlain(): void
    {
        $this->getResponse()->setContent(
            nl2br(
                str_replace(
                    ' ',
                    '&nbsp',
                    htmlentities(
                        print_r(
                            $this->purifyConfig(),
                            true)
                    )
                )
            )
        );
    }

    private function formatVardump(): void
    {
        $oldDepth    = ini_set('xdebug.var_display_max_depth', '-1');
        $oldChildren = ini_set('xdebug.var_display_max_children', '-1');
        $oldData     = ini_set('xdebug.var_display_max_data', '-1');

        ob_start();

        var_dump($this->purifyConfig());

        $varDump = ob_get_clean();

        ini_set('xdebug.var_display_max_depth', $oldDepth);
        ini_set('xdebug.var_display_max_children', $oldChildren);
        ini_set('xdebug.var_display_max_data', $oldData);

        $this->getResponse()->setContent($varDump);
    }
}
