<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Debug\Controller;

use FlyingAnvil\Scaffold\Application\Collection\ConfigCollection;
use FlyingAnvil\Scaffold\Application\Collection\VarsCollection;
use FlyingAnvil\Scaffold\Application\Controller\AbstractController;
use FlyingAnvil\Scaffold\Application\Router\Route;
use FlyingAnvil\Scaffold\Application\Router\RouteConfigToArray;

class RouteDebugController extends AbstractController
{
    /** @var RouteConfigToArray */
    private $routeConfigToArray;

    /** @var ConfigCollection */
    private $config;

    public function __construct(RouteConfigToArray $routeConfigToArray, ConfigCollection $config)
    {
        $this->routeConfigToArray = $routeConfigToArray;
        $this->config             = $config;
    }

    public function handle(VarsCollection $vars): void
    {
        $format       = $vars->get('format', 'json');
        $fieldsToShow = $this->getRequest()->getQueryParameters() ?: [];
        $routes       = $this->routeConfigToArray->convert($this->config->get(ConfigCollection::KEY_ROUTES));

        switch ($format) {
            case 'json':
                $this->formatJson($fieldsToShow, $routes);
                return;
            case 'plain':
                $this->formatPlain($fieldsToShow, $routes);
                return;
            case 'vardump':
                $this->formatVardump($fieldsToShow, $routes);
                return;
            default:
                $this->formatJson($fieldsToShow, $routes);
                return;
        }
    }

    private function convertRoutesToArray(array $routes, array $fields): array
    {
        if (!isset($fields['method'])
            && !isset($fields['handler'])
            && !isset($fields['route'])
            && !isset($fields['name'])) {
            $fields = [];
        }

        $formatted = [];

        /** @var Route $route */
        foreach ($routes as $route) {
            $current      = [];
            $routeName    = $route->getRouteName();
            $firstSegment = substr($routeName, 0, strpos($routeName, '/') ?: 0);

            if (empty($fields) || isset($fields['method'])) {
                $current['method'] = $route->getMethod();
            }

            if (empty($fields) || isset($fields['handler'])) {
                $current['handler'] = $route->getHandlerClass();
            }

            if (empty($fields) || isset($fields['route'])) {
                $current['route'] = $route->getRoute();
            }

            if (empty($fields) || isset($fields['name'])) {
                $current['name'] = $routeName;
            }

            if ($firstSegment === '') {
                $formatted[$routeName] = $current;
                continue;
            }

            $formatted[$firstSegment][$routeName] = $current;
        }

        return $formatted;
    }

    private function formatJson(array $fieldsToShow, array $routes): void
    {
        $this->jsonResponse($this->convertRoutesToArray($routes, $fieldsToShow));
    }

    private function formatPlain(array $fieldsToShow, array $routes): void
    {
        $this->getResponse()->setContent(
            nl2br(
                str_replace(
                    ' ',
                    '&nbsp',
                    htmlentities(
                        print_r(
                            $this->convertRoutesToArray($routes, $fieldsToShow),
                            true)
                    )
                )
            )
        );
    }

    private function formatVardump(array $fieldsToShow, array $routes): void
    {
        $oldDepth    = ini_set('xdebug.var_display_max_depth', '-1');
        $oldChildren = ini_set('xdebug.var_display_max_children', '-1');
        $oldData     = ini_set('xdebug.var_display_max_data', '-1');

        ob_start();

        var_dump($this->convertRoutesToArray($routes, $fieldsToShow));

        $varDump = ob_get_clean();

        ini_set('xdebug.var_display_max_depth', $oldDepth);
        ini_set('xdebug.var_display_max_children', $oldChildren);
        ini_set('xdebug.var_display_max_data', $oldData);

        $this->getResponse()->setContent($varDump);
    }
}
