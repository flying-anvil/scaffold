<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Debug;

use FlyingAnvil\Scaffold\Application\Application\ModuleInterface;

class DebugModule implements ModuleInterface
{
    public static function getConfig(): array
    {
        return require __DIR__ . '/../config/config.php';
    }

    public function init(): void
    {
    }
}
