<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Debug\SocketController;

use Exception;
use FlyingAnvil\Scaffold\WebSocket\Controller\AbstractSocketController;
use FlyingAnvil\Scaffold\WebSocket\DataObject\Client;

class ReturnMessageToSenderSocket extends AbstractSocketController
{
    public function onMessage(Client $client, string $message): void
    {
        $client->sendMessage($message);
    }

    public function onConnect(Client $client): void
    {
        $client->sendMessage('connected');
    }

    public function onDisconnect(Client $client): void
    {
        $client->sendMessage('disconnected');
    }

    public function onError(Client $client, Exception $exception): void
    {
        $client->sendMessage('wow, you caused an error!');
    }
}
