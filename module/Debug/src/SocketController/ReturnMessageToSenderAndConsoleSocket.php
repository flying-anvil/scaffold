<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\Debug\SocketController;

use FlyingAnvil\Scaffold\WebSocket\Controller\AbstractSocketController;
use FlyingAnvil\Scaffold\WebSocket\DataObject\Client;

class ReturnMessageToSenderAndConsoleSocket extends AbstractSocketController
{
    public function onMessage(Client $client, string $message): void
    {
        $client->sendMessage($message);

        $text = sprintf(
            '%s: "%s"',
            $client->getIdentifier(),
            $message
        );

        $this->getOutput()->writeLine($text);
    }

    public function onConnect(Client $client): void
    {
        $client->sendMessage('connected');

        $text = sprintf(
            '%s: connected',
            $client->getIdentifier(),
        );

        $this->getOutput()->writeLine($text);
    }

    public function onDisconnect(Client $client): void
    {
        $client->sendMessage('disconnected');

        $text = sprintf(
            '%s: disconnected',
            $client->getIdentifier(),
            );

        $this->getOutput()->writeLine($text);
    }

    public function onError(Client $client, \Exception $exception): void
    {
        $client->sendMessage('wow, you caused an error!');

        $text = sprintf(
            '%s: caused an error',
            $client->getIdentifier(),
        );

        $this->getOutput()->writeLine($text);
    }
}
