<?php

namespace FlyingAnvil\Scaffold\WebSocket;

use function DI\factory;
use FlyingAnvil\Scaffold\Application\Collection\ConfigCollection;
use FlyingAnvil\Scaffold\WebSocket\Controller\Factory\SocketControllerFactory;
use FlyingAnvil\Scaffold\WebSocket\Controller\SocketController;
use FlyingAnvil\Scaffold\WebSocket\Controller\SocketNotSupportedController;
use FlyingAnvil\Scaffold\WebSocket\Options\SocketOptions;
use FlyingAnvil\Scaffold\WebSocket\Router\Factory\SocketRouteDispatcherFactory;
use FlyingAnvil\Scaffold\WebSocket\Router\SocketRouteDispatcher;
use Ratchet\Server\IoServer;

if (!class_exists(IoServer::class)) {
//    echo 'Sockets are not supported, install "cboden/ratchet" ' .
//        '(composer require cboden/ratchet)';
    return [
        ConfigCollection::KEY_CLI => [
            'websocket' => [
                'handler'     => SocketNotSupportedController::class,
                'description' => 'Sockets are not supported, install "cboden/ratchet" ' .
                    '(composer require cboden/ratchet)',
            ]
        ],
    ];
}

return [
    ConfigCollection::KEY_DEPENDENCIES => [
        SocketController::class      => factory(SocketControllerFactory::class),
        SocketRouteDispatcher::class => factory(SocketRouteDispatcherFactory::class),
    ],
    ConfigCollection::KEY_CLI => [
        'websocket' => [
            'handler'     => SocketController::class,
            'description' => 'Starts a WebSocket whose behavior can be configured',
        ]
    ],
    ConfigCollection::KEY_SOCKETS => [
        SocketOptions::KEY_PORT        => 1414,
        SocketOptions::KEY_HTTP_PREFIX => 'ws',
    ]
];
