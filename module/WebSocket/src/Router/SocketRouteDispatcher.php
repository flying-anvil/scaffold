<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\WebSocket\Router;

use FlyingAnvil\Scaffold\Application\Application\CliOutput;
use FlyingAnvil\Scaffold\WebSocket\Controller\AbstractSocketController;
use FlyingAnvil\Scaffold\WebSocket\DataObject\Client;
use FlyingAnvil\Scaffold\WebSocket\Options\SocketOptions;
use GuzzleHttp\Psr7\Request;
use Psr\Container\ContainerInterface;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use React\EventLoop\TimerInterface;
use Throwable;

class SocketRouteDispatcher implements MessageComponentInterface
{
    /** @var ContainerInterface */
    private $container;

    /** @var CliOutput */
    private $output;

    /** @var array */
    private $config;

    /** @var string */
    private $httpPrefix;

    /** @var AbstractSocketController[] */
    private $controller = [];

    /** @var Client[] */
    private $clients;

    public function __construct(array $config, CliOutput $output, ContainerInterface $container)
    {
        $this->container = $container;
        $this->output    = $output;
        $this->config    = $config;

        $this->httpPrefix = trim($config[SocketOptions::KEY_HTTP_PREFIX] ?? 'ws', '/');
    }

    public function onOpen(ConnectionInterface $conn): void
    {
        try {
            /** @var Request $request */
            $request = $conn->httpRequest;
            $route = $request->getUri()->getPath();

            try {
                $handler = $this->getHandler($route);
            } catch (\Exception $exception) { // TODO: catch appropriate exception once it's there
                $this->output->writeLine($exception->getMessage());

                $data = [
                    'statusCode' => 500,
                    'messages'   => [
                        'there was server error'
                    ]
                ];

                $conn->send(json_encode($data));
                $conn->close();
                return;
            }

            if ($handler === null) {
                $data = [
                    'statusCode' => 404,
                    'messages'   => [
                        'route "' . $route . '" does not exist'
                    ]
                ];

                $conn->send(json_encode($data));
                $conn->close();
                return;
            }

            $ip     = $request->getHeader('X-Real-IP')[0] ?? 'unknown';
            $client = $this->getClient($conn, $ip);

            $handler->attachClient($client);
            $handler->onConnect($client);
            return;
        } catch (Throwable $throwable) {
            $this->output->writeLine('Critical Error: ' . $throwable . PHP_EOL);
            return;
        }
    }

    public function onClose(ConnectionInterface $conn): void
    {
        try {
            /** @var Request $request */
            $request = $conn->httpRequest;
            $route   = $request->getUri()->getPath();
            $handler = $this->getHandler($route);

            if ($handler === null) {
                $this->output->writeUnderline('Someone managed to close connection' .
                    ' without opening it first');
                return;
            }

            $ip     = $request->getHeader('X-Real-IP')[0] ?? 'unknown';
            $client = $this->getClient($conn, $ip);

            $handler->onDisconnect($client);
            $handler->detachClient($client);

            return;
        } catch (Throwable $throwable) {
            $this->output->writeLine('Critical Error: ' . $throwable . PHP_EOL);
            return;
        }
    }

    public function onError(ConnectionInterface $conn, \Exception $exception): void
    {
        try {
            /** @var Request $request */
            $request = $conn->httpRequest;
            $route   = $request->getUri()->getPath();
            $handler = $this->getHandler($route);

            if ($handler === null) {
                return;
            }

            $ip     = $request->getHeader('X-Real-IP')[0] ?? 'unknown';
            $client = $this->getClient($conn, $ip);

            $handler->onError($client, $exception);

            return;
        } catch (Throwable $throwable) {
            $this->output->writeLine('Critical Error: ' . $throwable . PHP_EOL);
            return;
        }
    }

    public function onMessage(ConnectionInterface $from, $msg): void
    {
        try {
            /** @var Request $request */
            $request = $from->httpRequest;
            $route   = $request->getUri()->getPath();
            $handler = $this->getHandler($route);

            if ($handler === null) {
                $this->output->writeUnderline('Someone managed to write a message' .
                    ' without opening a connection');
                return;
            }

            $ip     = $request->getHeader('X-Real-IP')[0] ?? 'unknown';
            $client = $this->getClient($from, $ip);

            $handler->onMessage($client, $msg);

            return;
        } catch (Throwable $throwable) {
            $this->output->writeLine('Critical Error: ' . $throwable . PHP_EOL);
            return;
        }
    }

    private function getClient(ConnectionInterface $connection, string $ip = 'unknown'): Client
    {
        $objectHash = spl_object_hash($connection);
        $client     = $this->clients[$objectHash] ?? null;

        if ($client === null) {
            $this->output->writeLine('new Client');
            $client = new Client($connection, $objectHash, $ip);
            $this->clients[$objectHash] = $client;
        }

        return $client;
    }

    private function getHandler(string $route)
    {
        $allRoutes    = $this->config[SocketOptions::KEY_ROUTES];
        $route        = substr($route, strlen('/' . $this->httpPrefix .'/'));
        $matchedRoute = $allRoutes[$route] ?? null;

        if ($matchedRoute === null) {
            return null;
        }

        if (isset($this->controller[$route])) {
            return $this->controller[$route];
        }

        $handlerName = $matchedRoute['handler'] ?? '';

        if (!is_subclass_of($handlerName, AbstractSocketController::class, true)) {
            // TODO: throw better exception
            throw new \Exception(sprintf(
                'Handler must implement "%s", "%s" does not',
                AbstractSocketController::class,
                $handlerName
            ));
        }

        /** @var AbstractSocketController $handler */
        $handler = $this->container->get($handlerName);
        $handler->prepare($this->output);
        $this->controller[$route] = $handler;

        return $handler;
    }

    public function doTick(TimerInterface $timer): void
    {
        foreach ($this->controller as $controller) {
            $controller->onTick();
        }
    }
}
