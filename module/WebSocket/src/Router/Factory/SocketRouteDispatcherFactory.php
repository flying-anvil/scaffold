<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\WebSocket\Router\Factory;

use FlyingAnvil\Scaffold\Application\Application\CliOutput;
use FlyingAnvil\Scaffold\Application\Collection\ConfigCollection;
use Psr\Container\ContainerInterface;
use FlyingAnvil\Scaffold\WebSocket\Router\SocketRouteDispatcher;

class SocketRouteDispatcherFactory
{
    public function __invoke(ContainerInterface $container): SocketRouteDispatcher
    {
        /**
         * @var ConfigCollection $allConfig
         */
        $allConfig    = $container->get(ConfigCollection::class);
        $socketConfig = $allConfig->get('sockets');
        $output       = $container->get(CliOutput::class);

        return new SocketRouteDispatcher($socketConfig, $output, $container);
    }
}
