<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\WebSocket\DataObject;

use Ratchet\ConnectionInterface;

class Client
{
    /** @var ConnectionInterface */
    private $connection;

    /** @var string */
    private $identifier;

    /** @var string */
    private $ip;

    public function __construct(ConnectionInterface $connection, string $identifier, string $ip = 'unknown')
    {
        $this->connection = $connection;
        $this->identifier = $identifier;
        $this->ip         = $ip;
    }

    public function sendMessage(string $message): void
    {
        $this->connection->send($message);
    }

    public function close(string $message = ''): void
    {
        if ($message !== '') {
            $this->connection->send($message);
        }

        $this->connection->close();
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getIp(): string
    {
        return $this->ip;
    }
}
