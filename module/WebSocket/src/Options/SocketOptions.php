<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\WebSocket\Options;

class SocketOptions
{
    public const KEY_PORT          = 'port';
    public const KEY_ROUTES        = 'routes';
    public const KEY_HTTP_PREFIX   = 'httpPrefix';
    public const KEY_TICK_INTERVAL = 'tickInterval';
}
