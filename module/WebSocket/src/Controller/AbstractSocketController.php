<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\WebSocket\Controller;

use Exception;
use FlyingAnvil\Scaffold\Application\Application\CliOutput;
use FlyingAnvil\Scaffold\WebSocket\DataObject\Client;
use SplObjectStorage;

abstract class AbstractSocketController
{
    /** @var CliOutput */
    private $output;

    /** @var SplObjectStorage<Client> */
    private $clients;

    abstract public function onMessage(Client $client, string $message): void;

    abstract public function onConnect(Client $client): void;

    abstract public function onDisconnect(Client $client): void;

    abstract public function onError(Client $client, Exception $exception): void;

    public function onTick(): void
    {
    }

    protected function jsonMessage($data): void
    {
        $json = json_encode($data, JSON_THROW_ON_ERROR);
        $this->output->write($json);
    }

    public function prepare(CliOutput $output): void
    {
        $this->output  = $output;
        $this->clients = new SplObjectStorage();
    }

    public function attachClient(Client $client)
    {
        $this->clients->attach($client);
        return $this;
    }

    public function detachClient(Client $client)
    {
        $this->clients->detach($client);
        return $this;
    }

    protected function getOutput(): CliOutput
    {
        return $this->output;
    }

    /**
     * @return SplObjectStorage<Client> | Client[]
     */
    protected function getClients(): SplObjectStorage
    {
        return $this->clients;
    }
}
