<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\WebSocket\Controller;

use FlyingAnvil\Scaffold\Application\Application\CliOutput;
use FlyingAnvil\Scaffold\Application\Collection\CliParams;
use FlyingAnvil\Scaffold\Application\Controller\AbstractCliController;

class SocketNotSupportedController extends AbstractCliController
{
    public function handleCli(CliParams $params, CliOutput $output): void
    {
        $output->writeLine('Sockets are not supported, install "cboden/ratchet" ' .
            '(composer require cboden/ratchet)');
    }
}
