<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\WebSocket\Controller\Factory;

use FlyingAnvil\Scaffold\Application\Collection\ConfigCollection;
use FlyingAnvil\Scaffold\WebSocket\Controller\SocketController;
use FlyingAnvil\Scaffold\WebSocket\Router\SocketRouteDispatcher;
use Psr\Container\ContainerInterface;

class SocketControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var SocketRouteDispatcher $router
         * @var ConfigCollection $allConfig
         */
        $router = $container->get(SocketRouteDispatcher::class);

        $allConfig    = $container->get(ConfigCollection::class);
        $socketConfig = $allConfig->get('sockets');

        return new SocketController($router, $socketConfig);
    }
}
