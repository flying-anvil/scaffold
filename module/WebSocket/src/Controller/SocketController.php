<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\WebSocket\Controller;

use FlyingAnvil\Scaffold\Application\Application\CliOutput;
use FlyingAnvil\Scaffold\Application\Collection\CliParams;
use FlyingAnvil\Scaffold\Application\Controller\AbstractCliController;
use FlyingAnvil\Scaffold\WebSocket\Options\SocketOptions;
use FlyingAnvil\Scaffold\WebSocket\Router\SocketRouteDispatcher;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Factory as LoopFactory;
use React\Socket\Server as Reactor;

class SocketController extends AbstractCliController
{
    /** @var SocketRouteDispatcher */
    private $router;

    /** @var array */
    private $config;

    public function __construct(SocketRouteDispatcher $router, array $config)
    {
        $this->router = $router;
        $this->config = $config;
    }

    public function handleCli(CliParams $params, CliOutput $output): void
    {
        $server = $this->buildIoServer();
        $output->writeLine();
        $output->writeUnderline('Socket started, ready to handle messages');
        $server->run();
    }

    private function buildIoServer(): IoServer
    {
        $httpServer = new HttpServer(
            new WsServer(
                $this->router
            )
        );

        $loop         = LoopFactory::create();
        $tickInterval = $this->config[SocketOptions::KEY_TICK_INTERVAL] ?? null;

        if ($tickInterval !== null) {
            $loop->addPeriodicTimer($tickInterval, [$this->router, 'doTick']);
        }

        $socket = new Reactor('0.0.0.0:' . ($this->config['port'] ?? '1414'), $loop);

        return new IoServer($httpServer, $socket, $loop);
    }
}
