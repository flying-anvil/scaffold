<?php

declare(strict_types=1);

namespace FlyingAnvil\Scaffold\WebSocket;

use FlyingAnvil\Scaffold\Application\Application\ModuleInterface;

class WebSocket implements ModuleInterface
{
    public static function getConfig(): array
    {
        return require __DIR__ . '/../config/config.php';
    }

    public function init(): void
    {
    }
}
