
---

## Feature

- Config Cache

- Rethink sockets
  - Maybe Controller as key
  - Or search whole config array for all Controllers
  - Instantiate all Controllers at start, so they all can tick
    - Individual tick interval for each controller

- Define route with regex for slug
  ```php
    '{date}' => [
      'method'  => 'GET',
      'handler' => some::class,
      'constraint (TBD)' => [
        'date'  => '\d{4}-\d{2}-\d{2}'
      ]
    ]
  ```

- Configurable cache for Controllers (maybe as module)

---

## Hotfix

- overriding relative route and name while using optional segments is currently broken
  - maybe rewrite the `RouteConfigToArray::buildRoutes` and use arrays of prefixes instead of strings?

---

## Misc

- update compose.json when finished

---
