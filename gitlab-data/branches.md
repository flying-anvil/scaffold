
---

## Feature

- [SKEL-1]: Routing and nested routes with some possibilities to define them
- [SKEL-2]: Loading environment based config files
- [SKEL-2]: Handle multiple modules
- [SKEL-3]: Preprocessors before controller (e.g. for authorization or caching)
- [SKEL-3]: Postprocessors after controller (e.g. for caching)
- [SKEL-4]: Calling a 404-Controller or 405-Controller on error during dispatching
- [SKEL-5]: Console usable Application
- [SKEL-6]: Web-Socket experiments
- [SKEL-7]: Update Readme
- [SKEL-7]: Finishing touches

[SKEL-1]: https://gitlab.com/flying-anvil/skeleton-web-server/tree/feature-SKEL-1
[SKEL-2]: https://gitlab.com/flying-anvil/skeleton-web-server/tree/feature-SKEL-2
[SKEL-3]: https://gitlab.com/flying-anvil/skeleton-web-server/tree/feature-SKEL-3
[SKEL-4]: https://gitlab.com/flying-anvil/skeleton-web-server/tree/feature-SKEL-4
[SKEL-5]: https://gitlab.com/flying-anvil/skeleton-web-server/tree/feature-SKEL-5
[SKEL-6]: https://gitlab.com/flying-anvil/skeleton-web-server/tree/feature-SKEL-6
[SKEL-7]: https://gitlab.com/flying-anvil/skeleton-web-server/tree/feature-SKEL-7

---

## Hotfix

---

## Misc

- [master]: stable branch with latest features

[master]: https://gitlab.com/flying-anvil/skeleton-web-server/tree/master

---
