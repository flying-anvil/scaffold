<?php

return [
    \FlyingAnvil\Scaffold\Application\Module::class,
    \FlyingAnvil\Scaffold\Debug\DebugModule::class,
    \FlyingAnvil\Scaffold\WebSocket\WebSocket::class,
];
