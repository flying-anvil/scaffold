<?php

use FlyingAnvil\Scaffold\Application\Controller\IndexController;

return [
    'index' => [
        'method'  => 'GET',
        'handler' => IndexController::class,
        'route'   => '/'
    ],

    // TODO: remove after tests
//    'global' => [
//        'method'  => 'GET',
//                'route' => 'json/{slug}',
//        'handler' => JsonRequestDebugController::class,
//    ],
//    'user' => [
//        'login' => [
//            'method'  => 'GET',
//            'handler' => LoginController::class,
//        ],
//        'register' => [
//            'method'  => 'GET',
//            'handler' => RegisterController::class,
//        ],
//    ]
];
