<?php

use FlyingAnvil\Scaffold\Application\Controller\ConfigDebugController;
use FlyingAnvil\Scaffold\Application\Controller\PlainRequestDebugController;
use FlyingAnvil\Scaffold\Application\Controller\RequestDebugController;
use FlyingAnvil\Scaffold\Application\Controller\RouteDebugController;
use FlyingAnvil\Scaffold\Application\Controller\VarDumpRequestDebugController;

return [
//    'debug' => [
//        'request' => [
//            'json' => [
//                'method'  => 'GET',
//                'handler' => JsonRequestDebugController::class,
//            ],
//            'plain' => [
//                'method'  => 'GET',
//                'handler' => PlainRequestDebugController::class,
//            ],
//            'vardump' => [
//                'method'  => 'GET',
//                'handler' => VarDumpRequestDebugController::class,
//            ],
//        ],
//        'config' => [
//            'json' => [
//                'method'  => 'GET',
//                'handler' => JsonConfigDebugController::class
//            ]
//        ],
//        'route-params' => [
//            '{name}' => [
//                'method'  => 'GET',
//                'handler' => JsonRequestDebugController::class,
//            ]
//        ],
//        'routes' => [
//            'method'  => 'GET',
//            'handler' => RouteDebugController::class,
//        ]
//    ],
];
